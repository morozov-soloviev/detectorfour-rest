﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DetectorFourRest.Models
{
    public class Measure
    {
        public Guid Id { get; set; }

        public DateTime? Date { get; set; }

        public string Name { get; set; }
        public string Post { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public string Note { get; set; }

        public double? LocationLatitude { get; set; }
        public double? LocationLongitude { get; set; }

        public double? Magneto { get; set; }

        public double? Presure { get; set; }

        public double? Vibration { get; set; }
    }
}
