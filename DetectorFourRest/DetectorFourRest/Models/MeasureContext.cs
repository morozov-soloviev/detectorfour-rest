﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DetectorFourRest.Models
{
    public class MeasureContext : DbContext
    {
        public DbSet<Measure> Measures { get; set; }

        public MeasureContext(DbContextOptions<MeasureContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
