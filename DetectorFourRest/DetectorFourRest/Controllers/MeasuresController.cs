﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DetectorFourRest.Models;

namespace DetectorFourRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MeasuresController : ControllerBase
    {
        private readonly MeasureContext _db;

        public MeasuresController(MeasureContext context)
        {
            _db = context;
        }

        // GET api/measures
        [HttpGet]
        public ActionResult<IEnumerable<Measure>> Get()
        {
            return _db.Measures;
        }

        // GET api/measures/{id}
        [HttpGet("{id}")]
        public ActionResult<Measure> Get(Guid id)
        {
            return _db.Measures.FirstOrDefault(p => p.Id == id);
        }

        // PUT api/measures
        [HttpPut]
        public void Put([FromBody] Measure measure)
        {
            if (_db.Measures.FirstOrDefault(p => p.Id == measure.Id) == null)
                _db.Measures.Add(measure);
            _db.SaveChanges();
        }

        // DELETE api/measures/{id}
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            Measure measure = _db.Measures.FirstOrDefault(p => p.Id == id);
            if (measure != null)
                _db.Measures.Remove(measure);
            _db.SaveChanges();
        }
    }
}
